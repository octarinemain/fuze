/* global SimpleBar */

'use strict';

$(document).ready(function () {
  // Parallax mousemove
  (function () {
    var images = $('.main-breeding__img');

    $(window).on('mousemove', function (e) {
      var w = $(window).width();
      var h = $(window).height();
      var offsetX = .5 - e.pageX / w;
      var offsetY = .5 - e.pageY / h;

      images.each(function () {
        var $this = $(this);
        var offset = $this.data('offset');
        var move = "translate(".concat(Math.round(offsetX * offset), "px, ").concat(Math.round(offsetY * offset), "px)");

        $this.css('transform', move);
      });
    });
  })();

  // Cookies
  (function () {
    var cookies = $('.cookies');

    if (getCookie('FUZE') === undefined) cookies.addClass('js-show');

    cookies.find('.btn--cookies').on('click', function (e) {
      e.preventDefault();
      cookies.removeClass('js-show');
      document.cookie = 'FUZE=value; path=/;';
    });

    function getCookie(name) {
      var matches = document.cookie.match(new RegExp("(?:^|; )".concat(
      name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1'), "=([^;]*)")));

      return matches ? decodeURIComponent(matches[1]) : undefined;
    }
  })();


  // Burger menu
  (function () {
    var header = $('#header');
    var burgerMenu = header.find('.burger-menu');

    burgerMenu.on('click', function (e) {
      e.preventDefault();
      var parent = $(this).parent();
      var menu = parent.find('.header__menu');
      menu.toggleClass('active');
      $('html').toggleClass('no-scroll');

      if (menu.hasClass('active') && $('html').hasClass('no-scroll')) {
        var width = $(window).width();

        $(window).on('resize.menu', function () {
          var widthResize = $(window).width();

          if (width !== widthResize) {
            menu.removeClass('active');
            $('html').removeClass('no-scroll');
            return $(window).off('resize.menu');
          }
        });
      } else {
        return $(window).off('resize.menu');
      }
    });

  })();


  // Select2
  (function () {
    var selects = $('select');

    selects.each(function () {
      var $this = $(this);
      var parent = $this.parent();
      var selectThems = parent.find('select[name="thems"]');
      var selectCitiy = parent.find('select[name="citiy"]');
      var selectRegion = parent.find('select[name="region"]');
      var selectStreet = parent.find('select[name="street"]');
      var selectHouse = parent.find('select[name="house"]');

      selectThems.select2({
        placeholder: 'Выберете тему',
        minimumResultsForSearch: -1,
        dropdownParent: parent });


      selectCitiy.select2({
        placeholder: 'Город',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Город не найден';
          } } });



      selectRegion.select2({
        placeholder: 'Регион',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Регион не найден';
          } } });



      selectStreet.select2({
        placeholder: 'Улица',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Улица не найдена';
          } } });



      selectHouse.select2({
        placeholder: '№ дома',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Дом не найден';
          } } });


    });
  })();


  // Profile card
  (function () {
    var profile = $('.profile-page');

    if (profile.length >= 1) rmClassForCard();

    function rmClassForCard() {
      var noCard = profile.find('.profile-page__inner--nocard');
      var card = profile.find('.profile-page__inner--checks');
      var addCard = profile.find('.profile-page__card .add-card');
      var addedCard = profile.find('.profile-page__card .added-card');

      if (noCard.hasClass('active')) {
        addCard.removeClass('hidden');
        addedCard.addClass('hidden');
      } else if (card.hasClass('active')) {
        addedCard.removeClass('hidden');
        addCard.addClass('hidden');
      }
    }
  })();


  // Init custom scroll
  (function () {
    var tableChecks = document.getElementById('table-checks');
    var tableWinners = document.getElementById('table-winners');

    if (tableChecks !== null) initCustomScroll(tableChecks);
    if (tableWinners !== null) initCustomScroll(tableWinners);

    function initCustomScroll(el) {
      return new SimpleBar(el, {
        autoHide: false });

    }
  })();


  // Set titles
  (function () {
    var tableChecks = document.getElementById('table-checks');

    function setTitles() {
      var confirmMobile = tableChecks.getElementsByClassName('profile-page__buy-inner--mobile');
      var confirmDesktop = tableChecks.getElementsByClassName('profile-page__confirm');

      for (var i = 0; i < confirmMobile.length; i++) {confirmMobile[i].textContent = confirmDesktop[i].textContent;}
    }

    if (tableChecks !== null) setTitles();
  })();


  // Popups
  (function () {
    var menu = $('#header').find('.header__menu');
    var btnOpenPopups = $('.js-popup-button');
    var btnClosePopups = $('.js-close-popup');

    function intPopup() {
      btnOpenPopups.on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('js-popup-show');
        var popupClass = ".".concat($(this).attr('data-popupshow'));
        $(popupClass).addClass('js-popup-show');

        if (!menu.hasClass('active')) hiddenScroll();
      });

      closePopup();
    }

    function closePopup() {
      btnClosePopups.on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('js-popup-show');

        if (!menu.hasClass('active')) visibleScroll();
      });
    }
    intPopup();
  })();

  function hiddenScroll() {
    if ($(document).height() > $(window).height()) {
      var scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
      $('html').addClass('no-scroll').css('top', -scrollTop);
    }
  }

  function visibleScroll() {
    var scrollTop = parseInt($('html').css('top'));
    $('html').removeClass('no-scroll');
    $('html, body').scrollTop(-scrollTop);
  }

  // Masks
  (function () {
    var phone = $('input[type="tel"]:not(.search)');
    var date = $('input[name="birthday"], input[name="issuing_date_document"]');

    phone.mask('+7 (999) 999-99-99', {
      autoclear: false });


    date.mask('99/99/9999', {
      autoclear: false });

  })();


  // Upload photo
  (function () {
    window.URL = window.URL || window.webkitURL;
    var wrap = $('#upload-photo');
    var inputsFile = wrap.find('input[type="file"]');

    inputsFile.on('change', function () {
      var $this = $(this);
      var parent = $this.parent();
      var photo = parent.find('.input-wrap__file-photo');
      var photoError = parent.find('.input-wrap__file-error');

      function handleFiles(files) {
        if (!files.length) {
          photoError.removeAttr('style');
          photo.removeAttr('style');
          $this.val('');
          return;
        }

        for (var i = 0; i < files.length; i++) {
          var photoPath = window.URL.createObjectURL(files[i]);
          photoError.css('display', 'none');
          photo.css({
            'display': 'block',
            'background-image': "url(".concat(photoPath, ")") });

        }
      }
      handleFiles(this.files);
    });
  })();

  // Scroll to
  (function () {
    var arrow = $('.main__arrow-inner');
    var header = $('#header');
    var nav = header.find('.nav');
    var prizeId = document.getElementById('prize-info');
    var winnersId = document.getElementById('winners');
    var menu = header.find('.header__menu');

    arrow.on('click', function () {
      var target = $(this).attr('href');

      scrollTo(target);
      return false;
    });

    if (prizeId !== null && winnersId !== null) nav.on('click', '.nav__item--scroll', navScroll);

    function navScroll() {
      var target = $(this).attr('href');

      if (menu.hasClass('active') && $('html').hasClass('no-scroll')) {
        menu.removeClass('active');
        $('html').removeClass('no-scroll');
      }

      scrollTo(target);
      return false;
    }

    function scrollTo(el) {
      $('html, body').stop().animate({
        scrollTop: $(el).offset().top },
      600, function () {
        location.hash = el;
      });
    }
  })();


  // Validate
  (function () {
    var forms = $('form');

    $.each(forms, function () {
      $(this).validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          phone: {
            required: true,
            phone: true },

          password: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          email: {
            required: true,
            email: true },

          name: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          surname: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          thems: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          card: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          citiy: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          birthday: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          registration: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          inn: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          series: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          number: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          issuing_date_document: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          name_of_issuing_authority: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          region: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          street: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          house: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          photo_passport: {
            required: true,
            extension: 'png|jpeg|jpg|gif|bmp',
            filesize: 5048576 },

          photo_registration: {
            required: true,
            extension: 'png|jpeg|jpg|gif|bmp',
            filesize: 5048576 },

          photo_inn: {
            required: true,
            extension: 'png|jpeg|jpg|gif|bmp',
            filesize: 5048576 } },


        messages: {
          phone: 'Некорректный номер',
          rules: {
            required: '' },

          personalAgreement: {
            required: '' },

          photo_passport: {
            required: '' },

          photo_registration: {
            required: '' },

          photo_inn: {
            required: '' } },


        highlight: function highlight(el) {
          var parent = $(el).closest('.input-wrap');
          parent.addClass('error');
        },
        unhighlight: function unhighlight(el) {
          var parent = $(el).closest('.input-wrap');
          parent.removeClass('error');
        } });


      jQuery.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /\+7\s\(\d+\)\s\d{3}-\d{2}-\d{2}/.test(value);
      });

      jQuery.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /\w+@[a-zA-Z_-]+?\.[a-zA-Z]{2,6}/.test(value);
      });

      jQuery.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || element.files[0].size <= param;
      });
    });
  })();
});